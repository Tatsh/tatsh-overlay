This is stuff I make randomly. There is no real (free) support whatsoever, but if you find a bug, please file an issue.

# To install with layman

```bash
layman -o https://raw.githubusercontent.com/Tatsh/tatsh-overlay/master/layman.xml -fa tatsh-overlay
```
